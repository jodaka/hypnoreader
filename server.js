/*global require, console, process */

var rss = ( function () {

    "use strict";

    var deferred  = require('deferred');
    var cfg       = require('config').cfg;
    var settings  = require('config').settings;

    var eventer   = require('./lib/eventer.js');
    var auth      = require('./lib/auth.js');
    var feeds     = require('./lib/feeds.js');
    var proxy     = require('./lib/proxy.js');
    var fs        = require('fs');

    var log       = require('logule').init( ( typeof module !== 'undefined' ) ? module : global.module, 'server' );

    var memInterval = setInterval( function() {

        log.trace( ' **  MEM ', process.memoryUsage() );

    }, 30 * 1000 );

    // if we are running in the plain client/server mode
    // will start websockets server to listen for incoming connections
    //
    // in node-webkit mode this isn't needed
    if ( typeof window === "undefined" ) {

        var httpServ = ( cfg.ssl ) ? require('https') : require('http');
        var WSServ   = require('ws').Server;
        var app      = null;

        // dummy request processing
        var processRequest = function( req, res ) {

            res.writeHead(200);
            res.end("All glory to the hypnoreader!\n");
        };

        if ( cfg.ssl ) {

            app = httpServ.createServer({

                // providing server with  SSL key/cert
                key: fs.readFileSync( cfg.ssl_key ),
                cert: fs.readFileSync( cfg.ssl_cert )

            }, processRequest ).listen( cfg.server_ws_port, 'localhost' );

        } else {

            app = httpServ.createServer( processRequest ).listen( cfg.server_ws_port, 'localhost' );
        }

        console.log( 'HTTP' + ( ( cfg.ssl ) ? 'S' : '' ) + ' server created' );

        var wss = new WSServ( { server: app } );

        console.log( 'Starting WebSocket server on top of it' );

        wss.on( 'connection', function ( wsConnect ) {

            log.info( ' !!!! connect' );
            // log.warn( wsConnect );

            wsConnect.on( 'message', function ( message ) {


                // checking for ping
                if ( message == 'ping' ) {
                    // that was ping
                    return;
                }

                var data = [];

                try {

                    data = JSON.parse( message );

                } catch( e ) {

                    log.warn(' got unparsed message', message );
                    wsConnect.send('fail');
                    return;
                }

                // Data must be array
                if ( ! ( data instanceof Array ) ) {
                    data = [ data ];
                }

                log.debug( ' wsConnect proxing event', data[0] );
                eventer.emitLocal.apply( eventer, data );

            });

            wsConnect.on( 'close', function () {

                log.info( ' !!!! disconnected' );

                eventer.emitLocal.apply( eventer, [ 'wsConnect-disconnect' ] );

                // app.close();
                wsConnect.terminate();
                // wss.close();

            });

            eventer.ws( wsConnect );
        });

        wss.on( 'error', function ( e ) {

            log.error( 'WS SERVER ERROR', e );
        });

    }


    // debugging
    deferred.monitor();

    auth.init({
        'events' : eventer
    });

    feeds.init({
        'events' : eventer,
        'auth': auth
    });

    proxy.init({
        'events' : eventer
    });

    return {
        eventer : eventer,
        settings: settings
    };

}());