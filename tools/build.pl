#!/usr/bin/perl

use strict;
use 5.10.0;
use Cwd;
use File::Path qw/rmtree/;
use File::Copy qw/copy/;

use strict;

my $cwd = getcwd();

use Data::Dumper qw/Dumper/;


my %cfg = (

    client_deploy_dir     => "app",
    client_deploy_subdirs => [ 'css', 'js', 'fonts', 'img' ],

    angular_app_name => 'hr',

    client_deploy_index => 'index.html',

    files_prefix_dir => "$cwd/client/build",

    index_template => "$cwd/client/index.tmpl",

    particles_dir => "$cwd/client/templates",
    particles_extension => "html",
    particles_prefix => 'client/templates/',
    particles_output => "$cwd/client/build/particles.js",

    files => {

        css => [
            'hypnoreader.rel.css'
        ],

        js => [
            'hypnoreader.rel.js',
            'client/js/lib/IndexedDBShim.min.js',
            'client/js/lib/angular.min.js'
        ],

        fonts => [
            'client/fonts/fontcustom.eot',
            'client/fonts/fontcustom.svg',
            'client/fonts/fontcustom.ttf',
            'client/fonts/fontcustom.woff'
        ],

        img => [
            'client/img/light.png'
        ]

    }
);

sub get_version( ) {

    if ( ! -e "$cwd/version" ) {

        say "Can't find version in current catalog $cwd" ;
        exit();
    }


    # get build version number
    if ( open ( my $VER, "<", "$cwd/version" ) ) {

        my $version = <$VER>;

        chomp( $version );

        close ( $VER );

        return $version;

    }

}


sub set_version
{
    if ( open ( my $VER, ">", "$cwd/version" ) ) {

        print $VER $_[0];

        close ( $VER );
    }
}

# removing old dirs and create new ones
sub create_deploy_dirs
{
    if ( -d "$cwd/$cfg{'client_deploy_dir'}" ) {
        say " removing ", $cfg{client_deploy_dir};
        rmtree "$cwd/$cfg{'client_deploy_dir'}";
    }

    mkdir "$cwd/$cfg{client_deploy_dir}";

    foreach ( @{ $cfg{'client_deploy_subdirs'} } ) {
        mkdir "$cwd/$cfg{client_deploy_dir}/$_";
    }

    `touch $cwd/$cfg{client_deploy_dir}/.empty`;


    say "Directory structure created";
    return 1;
}

sub create_index_templates
{

    my $buildnum = shift;
    my $buildtype = shift || 'dev';

    if ( open ( my $TMPL, "<", $cfg{'index_template'} ) ) {

        my $template = '';

        while ( <$TMPL> ) {
            chomp();
            s/^\s*|\s*$//g;
            $template .= $_;
        }

        close $TMPL;

        $template =~ s/\%buildnum\%/$buildnum/g;
        $template =~ s/\%buildtype\%/$buildtype/g;

        if ( open ( my $IDX, ">", "$cfg{'client_deploy_dir'}/$cfg{'client_deploy_index'}" ) ) {

            print $IDX $template;

            close $IDX;

            say "index template written";

        } else {

            die " Cant' write to index file $cfg{'client_deploy_dir'}/$cfg{'client_deploy_index'}: $!";
        }
    }
}


sub compile_angular_particles
{

    say "   > Particles compilation";

    my $templates_cache = "angular.module('$cfg{angular_app_name}').run( [ '\$templateCache', function preloadCache( \$templateCache ) {";

    if ( opendir ( my $DIR, $cfg{'particles_dir'} ) ) {

        while ( my $file = readdir( $DIR ) ) {

            if ( $file =~ /$cfg{particles_extension}$/ ) {

                if ( open ( my $particle, "<", "$cfg{'particles_dir'}/$file" ) ) {

                    say "     -> compiling: $file ";

                    $templates_cache .= "\n\t\$templateCache.put('$cfg{particles_prefix}$file', ";

                    my $particle_content = '';

                    while ( <$particle> ) {
                        chomp();
                        s/"/\\"/g;
                        s/^\s*|\s*$//g;
                        $particle_content .= $_;
                    }

                    $templates_cache .= '"' . $particle_content . '"' . ");";

                } else {
                    die( " Can't read particle $cfg{'particles_dir'}/$file: $!" )
                }

            }

        }

        $templates_cache .= "\n}]);";

        if ( open( my $res, ">", $cfg{'particles_output'} ) ) {

            print $res $templates_cache;

            close $res;

            say "   < particles compiled"
        }


    } else {
        die( " Can't open particles directory $cfg{'particles_dir'}: $!" )
    }

}

sub copy_files
{

    foreach my $group ( keys %{ $cfg{'files'} } ) {

        say " ==> $group";

        foreach my $file ( @{ $cfg{'files'}{ $group } } ) {

            my $path = ( $file =~ /\// ) ? $file : "$cfg{'files_prefix_dir'}/$file";

            if ( -e $path ) {

                if (! copy( "$path", "$cwd/$cfg{'client_deploy_dir'}/$group/" ) ) {
                    die ( "      can't copy $path --> $cwd/$cfg{'client_deploy_dir'}/$group/ :", $! );

                } else {
                    say "       $file copied"
                }
            } else {

                say "      can't find file : $path ";
            }

        }


    }

}

my $version;

sub run
{

    if ( $ARGV[0] eq 'particles' ) {

        compile_angular_particles();

    } else {


        $version = get_version();

        say " We are going to build build number ", $version + 1;

        create_deploy_dirs();

        create_index_templates( $version + 1, $ARGV[1] || 'dev' );

        set_version( $version + 1 );

        compile_angular_particles();

        copy_files();

    }

}


run();