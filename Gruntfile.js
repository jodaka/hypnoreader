/*global module:false*/
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        meta: {

            banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> */\n'

        },

        shell: {

            cleanBuild: {
                command: 'rm -f client/build/*'
            },

            copyConfig: {

                command: 'cp client/config/' + process.env.NODE_ENV + '.json client/build/config.json',
                options: {
                    stdout: true
                }
            },

            compileParticles: {

                command: './tools/build.pl particles',
                options: {
                    stdout: true
                }
            }
        },

        concat: {

            deps: {

                src: [
                    'client/js/lib/angular-sanitize.min.js',
                    'client/js/lib/events.js',
                    'client/js/lib/idbstore.js'
                ],

                dest: 'client/build/deps.concat.js'
            },

            client: {

                src: [
                    'client/js/app.js',
                    'client/js/services/*.js',
                    'client/js/filters/*.js',
                    'client/js/directives/*.js',
                    'client/js/controllers/*.js'
                ],

                dest: 'client/build/client.concat.js'
            },

            dev: {

                src: [
                    '<%= concat.deps.dest %>',
                    '<%= concat.client.dest %>',
                    'client/js/bootstrap.js'
                ],

                dest: 'client/build/hypnoreader.dev.js'
            },

            release: {

                src: [
                    'client/build/deps.min.js',
                    'client/build/client.min.js',
                    'client/build/bootstrap.min.js',
                    'client/js/lib/stats.min.js'
                ],

                dest: 'client/build/hypnoreader.rel.js'
            }

        },

        includes: {

            jsConfig: {

                options: {

                    includeRegexp: /\s*\/\/\s*@include\s"(\S+)"\s*$/,
                    duplicates: false,
                    debug: true

                },

                files: {
                    src : [ '<%= concat.client.dest %>' ],
                    dest: '.'
                }

            }

        },

        less: {

            production: {

                options: {

                    paths        : [
                        "client",
                        "client/css",
                        "client/css/blocks"
                    ],

                    flatten      : false,
                    strictImports: true
                },

                files: {
                    "client/build/hypnoreader.dev.css": "client/css/app.less"
                }
            },

        },

        removelogging: {

            client: {
                src: '<%= concat.client.dest %>',
                dest: '<%= concat.client.dest %>',
                options: {
                    replaceWith: "0;",
                    methods: [ 'log', 'warn' ]
                }
            },

            deps: {
                src: '<%= concat.deps.dest %>',
                dest: '<%= concat.deps.dest %>',
                options: {
                    replaceWith: "0;",
                    methods: [ 'log', 'warn' ]
                }
            }
        },

        uglify: {

            deps: {

                files: {
                    'client/build/deps.min.js': [
                        '<%= concat.deps.dest %>'
                    ]
                }
            },

            client: {

                files: {
                    'client/build/client.min.js': [
                        '<%= concat.client.dest %>'
                    ]
                }
            },

            bootstrap: {
                files: {
                    'client/build/bootstrap.min.js': [
                        'client/js/bootstrap.js'
                    ]
                }
            }
        },

        cssmin: {
            'client/build/hypnoreader.rel.css': "client/build/hypnoreader.dev.css"
        },

        watch: {

            js: {

                files: [
                    'client/js/app.js',
                    'client/js/services/*.js',
                    'client/js/filters/*.js',
                    'client/js/directives/*.js',
                    'client/js/controllers/*.js'
                ],

                tasks: [ 'concat:deps', 'concat:client', 'concat:dev' ]
            },

            particles: {

                files: [
                    'client/templates/*.html'
                ],

                tasks: [ 'shell:compileParticles' ]

            },

            css: {

                files: [
                    'client/css/blocks/**/*.less',
                    'client/css/*.less'
                ],

                tasks: [ 'less' ]
            }

        },

        jshint: {

            options: {
                curly  : true,
                eqeqeq : true,
                immed  : true,
                latedef: true,
                newcap : true,
                noarg  : true,
                sub    : true,
                undef  : true,
                boss   : true,
                eqnull : true,
                browser: true
            },

            globals: {
                jQuery : false,
                console: true
            }
        }

    });

    grunt.loadNpmTasks('grunt-includes-basic');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks("grunt-remove-logging");

    grunt.registerTask( 'default', [
        'shell:cleanBuild',
        'shell:copyConfig',
        'concat:deps',
        'concat:client',
        'includes',
        'concat:dev',
        'less'
    ]);

    grunt.registerTask( 'release', [
        'shell:cleanBuild',
        'shell:copyConfig',
        'shell:compileParticles',

        'uglify:bootstrap',

        'concat:deps',
        'removelogging:deps',
        'uglify:deps',

        'concat:client',
        'removelogging:client',
        'includes',
        'uglify:client',

        'concat:release',

        'less',
        'cssmin'
    ]);

};
