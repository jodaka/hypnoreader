angular.module( 'hr' ).filter( "specialChars", function () {

    return function ( text ) {

        if ( ! text ) {
            return '';
        }

        if ( typeof text.replace === 'undefined' ) {
            console.warn( text );
            return text;
        }

        text = text.replace( /&amp;/g, "&" );
        text = text.replace( /&#39;/g, "'" );

        return text;
    };

});

