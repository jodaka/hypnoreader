// converting raw url to google favicon url
angular.module( 'hr' ).filter( "stripName", function () {

    return function ( name ) {

        if ( typeof name === "undefined" ) {
            return '';
        }

        if ( name.length < 80 ) {
            return name;
        }

        name = name.slice( 0, 80 );

        var lastSpace = name.lastIndexOf( ' ' );

        if ( lastSpace !== -1 ) {
            name = name.slice( 0, lastSpace );
        }

        return name + '…';

    };

});
