// converting raw url to google favicon url
angular.module( 'hr' ).filter( "formatAtomTime", function () {

    // TODO
    // fix timezone
    return function ( ts ) {

        var d = new Date( ts * 1000 );
        var hours = d.getHours();
        var mins  = d.getMinutes();

        if ( hours < 10 ) {
            hours = '0' + hours;
        }

        if ( mins < 10 ) {
            mins = '0' + mins;
        }

        return hours + ':' + mins;

    };

});
