angular.module( 'hr' ).filter( "stripTeaser", function () {

    return function ( atom ) {

        var content = atom.content;

        if ( typeof content === 'undefined' || content.length === 0 ) {
            return '';
        }

        var iterations = 0;
        var re = /<.*?>/;

        // we don't really need to strip all tags… just first 50 or smth
        while ( re.test( content ) && iterations < 50 ) {

            content = content.replace( re, '' );
            iterations++;

        }

        return ( content.length <= 120 ) ? content : content.slice( 0, 120 ) + '…';

    };

});
