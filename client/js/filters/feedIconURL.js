
// converting raw url to google favicon url
angular.module( 'hr' ).filter( 'feedIconURL', function () {

    return function ( url ) {

        var a = document.createElement( 'a' );
        a.href= url;
        return 'http://s2.googleusercontent.com/s2/favicons?domain=' + a.hostname + '&alt=feed';

    };

});
