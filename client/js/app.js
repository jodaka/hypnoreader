/*global rss, console, angular */

angular.module( 'hr', [ 'ngSanitize' ]).run([ '$rootScope', function runApp( $rootScope ) {

    // authorization flag
    $rootScope.authorized = false;
    $rootScope.initializing = true;

    // @include "config.json"

}]);

// templates cache
// @include "particles.js"
