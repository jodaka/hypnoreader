angular.module( 'hr' ).directive( 'feedsList', [
    'eventer',
    function atomsListFactory( eventer ) {

    var listHolder           = null;



    var initFeedsList = function ( list ) {

        listHolder           = document.querySelector('#feeds .scroll');

        console.log('------');
    };


    var preprocessData = function ( scope, data ) {

        console.log( 'scope ', scope, ' data=', data );

        return data;

    };


    return {

        restrict: 'E',
        scope: true,

        link: function( scope, el, attrs ) {

            scope.$watch( 'feeds', function ( newVal, oldVal ) {

                if ( newVal === oldVal ) {
                    return;
                }
                // console.log( 'ffound new days, going to process them ');

                // this will force initializeData to be run
                // after template drawing
                setTimeout( function () {

                    console.warn( newVal );

                    initFeedsList( el );

                }, 0 );

                return preprocessData( scope, newVal );

            });


            /**
             * TODO
             */
            var selectFeed = function ( direction ) {

                var candidates = listHolder.querySelectorAll('.kbd');
                console.log( 'candidates', candidates );

                var currentIdx = null;

                for ( var i = candidates.length - 1; i >= 0; i-- ) {

                    if ( /KbdFocus/.test( candidates[ i ].className ) ) {
                        currentIdx = i;
                        break;
                    }

                }


                if ( direction === 'next' && currentIdx < candidates.length ) {

                    var el = angular.element( candidates[ currentIdx + 1 ].querySelector('.name') );

                    console.log( el, candidates[ currentIdx + 1 ], candidates[ currentIdx + 1 ].querySelector('.name') );
                    el.triggerHandler('click');

                }


                if ( direction === 'prev' && currentIdx > 0 ) {

                    var el = angular.element( candidates[ currentIdx - 1 ].querySelector('.name') );

                    console.log( el );
                    el.triggerHandler('click');

                }


                console.log( 'current selected element index = ', currentIdx );


                console.log( 'selecting ' + direction + ' feed');
            };

            // prev feed
            eventer.on( 'SelectPrevFeed', function () {
                selectFeed('prev');
            });

            eventer.on( 'SelectNextFeed', function () {
                selectFeed('next');
            });

        },

        replace   : true,
        templateUrl: 'client/templates/feedsList.html'
    };

}]);
