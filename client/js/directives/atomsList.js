angular.module( 'hr' ).directive( 'atomsList', [
    'eventer',
    function atomsListFactory( eventer ) {

    var list           = document.querySelector( '#atoms .scroll' );
    var curentDayTitle = document.getElementsByClassName( 'day-title-current' )[0];

    var prevDay    = null;
    var currentDay = null;
    var nextDay    = null;

    var days       = [];
    var dayIndex   = 0;

    var atomHeight = null; // will initialize later

    var DEBUG = true;

    /**
     * Will make a CSS animation of atomsList scrolling it up/down
     */
    var smoothScroll = function ( direction ) {

        if ( atomHeight === null ) {
            atomHeight = document.querySelector('#atoms .atom').clientHeight;
        }

        var scrollable = list.querySelector('#atoms-lst-test');

        var val = ( direction === 'next' ) ? -atomHeight : atomHeight;

        var activeItemTop = list.querySelector('.active').offsetTop;

        // middle
        var centerPoint = list.clientHeight / 2 + list.scrollTop - Math.abs( val ) / 2;
        var transitionTo = centerPoint - activeItemTop;

        // console.log('middle = ', centerPoint, ' (', list.clientHeight / 2 , list.scrollTop, ')');
        // console.log('transition to ', transitionTo);
        // console.log(' going to scroll TO ', scrollTo );
        // console.log(' math ', activeItemTop,  val, list.scrollTop );

        if ( scrollable.scrollHeight - list.clientHeight  > centerPoint + transitionTo ) {
            if (DEBUG) console.log(' YES ',scrollable.scrollHeight - list.clientHeight , ' > centerPoint + transitionTo ', centerPoint + transitionTo, '    ( ', centerPoint , transitionTo, ' )' );
        }

        if ( list.scrollTop - transitionTo > 0 || list.scrollTop + list.clientHeight < list.scrollHeight - list.clientHeight / 2 ) {

            // console.log('initial scroll top ', list.scrollTop );

            var handleCSSTransition = function( evt ){

                scrollable.removeEventListener( 'webkitTransitionEnd', handleCSSTransition, false );
                scrollable.removeEventListener( 'mozTransitionEnd', handleCSSTransition, false );
                scrollable.removeEventListener( 'transitionEnd', handleCSSTransition, false );

                scrollable.style['transition'] = 'transform 0 linear';
                scrollable.style['transform'] = 'translate3d(0, 0, 0)';

                scrollable.style['-webkit-transition'] = '-webkit-transform 0 linear';
                scrollable.style['-webkit-transform'] = 'translate3d(0, 0, 0)';

                list.scrollTop +=  -transitionTo ;

                // console.log(' ===> res top ', list.scrollTop );

            };

            // TODO add shims for other browsers
            scrollable.addEventListener('webkitTransitionEnd', handleCSSTransition, false );
            scrollable.addEventListener('mozTransitionEnd', handleCSSTransition, false );
            scrollable.addEventListener('transitionEnd', handleCSSTransition, false );

            scrollable.style['transform'] = 'translate3d(0, '+transitionTo+'px, 0)';
            scrollable.style['transition'] = 'transform 500ms ease';

            scrollable.style['-webkit-transform'] = 'translate3d(0, '+transitionTo+'px, 0)';
            scrollable.style['-webkit-transition'] = '-webkit-transform 500ms ease';

        }

    };

    eventer.on( 'scroll-atoms-list', smoothScroll );

    /**
     * On scroll event we apply special class .current to the currently active day element
     */
    list.addEventListener( 'scroll', function handleScrolling( evt ) {

        if ( nextDay && list.scrollTop > nextDay.top - 10 ) {

            prevDay    = currentDay;
            currentDay = nextDay;

            if ( days.length > dayIndex ) {

                nextDay = days[ ++dayIndex ];

            } else {
                nextDay = null;
            }

            angular.element( prevDay.el ).removeClass( 'current' );
            angular.element( currentDay.el ).addClass( 'current' );

            curentDayTitle.innerHTML = currentDay.el.innerHTML;
        }


        if ( prevDay && list.scrollTop < currentDay.top ) {

            angular.element( currentDay.el ).removeClass( 'current' );

            nextDay    = currentDay;
            currentDay = prevDay;

            angular.element( currentDay.el ).addClass( 'current' );
            curentDayTitle.innerHTML = currentDay.el.innerHTML;

            if ( dayIndex > 0 ) {

                prevDay = days[ --dayIndex ];

            } else {

                prevDay = null;
            }

        }


    });

    eventer.on( 'collapse-feeds-panel', function showHideFeedsPanel ( action ) {
        if ( currentDay ) {
            angular.element( currentDay.el ).toggleClass( 'collapsed', action );
        }
    });

    /**
     * Getting list of day-titles and their positions
     */
    var initializeData = function ( el ) {

        if (DEBUG) console.log('initializeData ');

        // removing current from old selection
        if ( currentDay ) {
            angular.element( currentDay.el ).removeClass('current');
        }

        var titles = el[ 0 ].querySelectorAll( '.day-title' );

        days = [];

        for ( var i = 0, iLen = titles.length; i < iLen; i++ ) {

            days.push({
                top: titles[i].offsetTop,
                el : titles[i]
            });
        }

        if ( days.length > 0 ) {

            prevDay  = null;
            dayIndex = 0;

            currentDay = days[ 0 ];
            if (DEBUG) console.log('setting current day to  ', currentDay );

            nextDay = ( days.length > 1 )
                ? days[1]
                : null;

            dayTitleHeight = days[0].el.clientHeight;

            curentDayTitle.innerHTML = days[0].el.innerHTML;


        }

        if (DEBUG) console.log(' hiding current day  ', currentDay.el );
        angular.element( currentDay.el ).addClass('current');

    };


    return {

        restrict: 'E',

        scope: true,

        link: function( scope, el, attrs ) {

            scope.$watch( 'days', function ( newVal, oldVal ) {

                if ( newVal === oldVal ) {
                    return;
                }
                // console.log( 'ffound new days, going to process them ');

                // this will force initializeData to be run
                // after template drawing
                setTimeout( function ( ) {
                    initializeData( el );
                }, 0 );
            });

        },

        replace   : false,
        templateUrl: 'client/templates/atomsList.html'
    };

}]);
