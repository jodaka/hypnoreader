angular.module( 'hr' ).directive( 'atomContent', [
    'eventer', '$http', '$templateCache',
    function factoryOriginalArticle( eventer, $http, $templateCache ) {

    var loadingHTML;

    // loading template from cache
    $http({ method: 'GET', url: 'client/templates/iframeLoading.html', cache: $templateCache })
    .success( function tmplLoaded( data ) {
        loadingHTML = data;
    });

    /**
     * TODO FIXME
     *
     * [optimizeFormatting description]
     * @param  {[type]} article [description]
     * @return {[type]}         [description]
     */
    var optimizeFormatting = function ( html ) {

        var node = document.createElement( 'div' );
        node.innerHTML = html;

        var allNodes = node.querySelectorAll( '*' );

        for ( var i = 0, iLen =  allNodes.length; i < iLen; i++ ) {
            allNodes[i].removeAttribute( 'style' );
        }

        // we intentionally remove width/height so images could
        // be resized properly
        var images = Array.prototype.slice.call( node.getElementsByTagName( 'img' ), 0 );

        for ( i = 0, iLen =  images.length; i < iLen; i++ ) {

            var h = images[i].height;
            var w = images[i].width;

            if ( h === 1 || h === 0 || w === 1 || w === 0 ) {

                console.log( 50, 'removed image that was too small from ', images[i].src );
                console.log( images[i] );
                // images[i].parentNode.removeChild( images[i] );
            } else {

                images[i].removeAttribute( 'width' );
                images[i].removeAttribute( 'height' );
            }

        }

        var links = node.getElementsByTagName( 'a' );

        for ( i = 0, iLen =  links.length; i < iLen; i++ ) {

            links[i].setAttribute( 'data-uri', links[i].href );
            links[i].removeAttribute( 'onlick' );
            // links[i].setAttribute( 'target', '_blank' );
            // links[i].setAttribute( 'ng-click', 'linkClick()' );
            // links[i].href = 'javascript:void(0)';
        }

        return node.innerHTML;
    };


    /**
     * [renderPageContents description]
     * @param  {[type]} content [description]
     * @return {[type]}         [description]
     */
    var renderPageContents = function ( params ) {

        var iframe = document.getElementById( 'content-original' );

        if ( typeof iframe.contentWindow.document === 'undefined' ) {

            var newIframe = document.createElement('iframe');
                newIframe.id = 'content-original';
                newIframe.setAttribute( 'sandbox', 'allow-scripts allow-same-origin allow-forms' );

            iframe.parentNode.replaceChild( newIframe, iframe );
            iframe = newIframe;

        }

        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write( params.content );
        iframe.contentWindow.document.close();
    };


    return {

        restrict: 'E',

        scope: true,

        link: function( scope, element, attrs ) {

            // console.warn( 777, element );

            var contentScroll = document.querySelector( '#content .scroll' );

            /**
             * Links handler
             * @param  {[type]} evt  [description]
             * @param  {[type]} elem
             * @param  {[type]} url
             * @return {[type]}      [description]
             */
            element.bind( 'click', function ( evt ) {

                if ( evt.target.tagName === 'A' ) {

                    evt.preventDefault();

                    var url = evt.target.getAttribute( 'data-uri' );
                    console.log( 'url of link is url ', url );

                    scope.loadOriginal( 'open', url );

                }

                return false;
            });


            scope.$watch( 'article',

                function ( content ) {

                    if ( typeof content === 'undefined' ) {
                        return;
                    }

                    // console.log( 50, ' updating article ', content );

                    content.text = optimizeFormatting( content.text );

                    contentScroll.scrollTop = 0;

                    return content;
                });

            scope.$watch( 'action',

                function ( action ) {

                    if ( typeof action === 'undefined' ) {
                        return;
                    }

                    console.log( 50, ' got action ', action );
                    scope.loadOriginal( 'close' );

                    return action;
                });

            scope.eventer.on( 'proxy-page-loaded', renderPageContents );

            // closing original view when user press hotkeys or click an another atom
            scope.eventer.on( 'switch-original', function ( action ) {

                if ( action === 'close' && scope.originalView ) {
                    scope.loadOriginal( 'close' );
                }
            });


            /**
             * [viewOriginal description]
             * @return {[type]} [description]
             */
            scope.loadOriginal = function ( action, url ) {

                scope.article.visible = ( action === 'open' ) ? false : true;
                scope.article.showOriginal = ( action === 'open' ) ? true : false;

                url = url || scope.article.original;

                if ( action === 'open' ) {

                    scope.originalView = true;

                    renderPageContents( { content: loadingHTML, charset: 'utf-8' });
                    console.log('going to open url ', url );
                    eventer.emit( 'proxy-page', url );
                    eventer.emitLocal( 'collapse-feeds', true );

                } else {

                    if ( scope.originalView ) {

                        console.log(' cleaning iframe' );

                        renderPageContents( { content: '', charset: 'utf-8' } );

                        scope.originalView = false;
                        eventer.emitLocal( 'collapse-feeds', false );

                    }
                }


                scope.applyProxy();
            };


            eventer.on( 'articleCloseOriginal', function () {
                scope.loadOriginal( 'close' );
            });

            eventer.on( 'articleViewOriginalInline', function () {
                scope.loadOriginal( 'open' );
            });

            /**
             * Open article in browser ( in another tab)
             * @return {[type]} [description]
             */
            scope.openInBrowser = function ( url ) {

                url = url || scope.article.original;

                window.open( url, '_blank' );
                window.focus();

            };

            eventer.on( 'articleViewOriginal', scope.openInBrowser );

        },

        replace    : true,
        templateUrl: 'client/templates/atomContent.html'
    };

}]);