angular.module( 'hr' ).directive( 'animate', function ( ) {

    return {

        restrict: 'A',

        link: function ( scope, element, attrs ) {

            setTimeout( function ( ) {
                element.addClass( attrs.animate );
            }, 0 );

        }
    };

});
