angular.module( 'hr' ).controller( 'LoginCtrl', [
    '$scope', '$rootScope', 'safeApply', 'eventer',
    function LoginCtrlFactory( $scope, $rootScope, safeApply, eventer ) {

    var credentials = {};

    $scope.showLoginForm = true;
    $scope.connected     = false;
    $scope.loading       = false;

    var statuses = {
        online : 'I can has sign in',
        offline: 'No connection to server',
        loading: 'Loading'
    };

    var credsDiv = document.getElementById( 'credentials' );
    angular.element( credsDiv ).removeClass( 'hdn' );

    $scope.submitStatus  = statuses.offline;

    /**
     * [checkCredentials description]
     * @return {[type]} [description]
     */
    var checkCredentials = function ( ) {

        // console.log( 2, 'checking credentials ' );

        $scope.showLoginForm = true;
        credentials = localStorage.getItem( 'credentials' );

        if ( ! credentials ) {

            // console.log( 2, 'no credentials stored' );
            $rootScope.initializing = false;

        } else {

            if ( typeof credentials === "string" ) {

                try {

                    // console.log( 2, 'found credentials', credentials, ' Going to parse' );
                    credentials = JSON.parse( credentials );

                } catch ( e ) {

                    console.error( 2, 'Bad credentials format ', e );
                }
            }

            if ( credentials.tokens && credentials.login && credentials.passw && credentials.ts ) {

                // checking timestamp
                if ( credentials.ts + ( 28 * 60 * 1000 ) > Date.now() ) {

                    $scope.showLoginForm = false;
                    eventer.emit( 'check-token', credentials.tokens );
                } else {

                    // timestamp expired. Let's request new tokens!
                    eventer.emit( 'authorize', { login: credentials.login, password: credentials.passw } );
                    $scope.loading = true;
                    $scope.submitStatus = statuses.loading;
                }

            } else {

                $scope.showLoginForm    = true;
                $scope.loading          = false;
                $rootScope.initializing = false;

                console.warn( 'Incomplete credentials. Need to login', credentials );
            }

        }

        safeApply.run();

    };

    var showLoginError = function ( msg ) {

        $scope.error = msg;
        $scope.displayError = true;

        safeApply.run();

        setTimeout( function (){
            $scope.displayError = false;
            $scope.error = '';
            safeApply.run();
        }, 3000 );
    };

    /**
     * [authGranted description]
     * @param  {[type]} tokens [description]
     * @return {[type]}        [description]
     */
    var authGranted = function ( tokens ) {

        // console.log( 2, 'authGranted with tokens ' );

        // tell rootScope that initialization is complete
        $rootScope.initializing = false;

        $scope.showLoginForm = false;
        $scope.loading       = false;

        if ( tokens ) {

            $scope.tokens        = tokens;

            // console.warn( 2, 'STORING CREDS ', {
            //     login : credentials.login,
            //     passw : credentials.passw,
            //     tokens: $scope.tokens
            // });

            localStorage.setItem( 'credentials', JSON.stringify({
                login : credentials.login,
                passw : credentials.passw,
                tokens: $scope.tokens,
                ts    : Date.now()
            }));
        }

        if ( ! $scope.authorized ) {

            $scope.authorized = true;
            eventer.emit( 'auth-granted' );
            // console.log( 2, 'auth-granted event emitted');
        }

        safeApply.run();
    };

    /**
     * [checkPasswd description]
     * @return {[type]} [description]
     */
    $scope.checkPasswd = function ( ) {

        // this string is soooo important.
        credentials = {};

        credentials.login = $scope.login || '';
        credentials.passw = $scope.password || '';

        if ( credentials.login === '' || credentials.passw === '' ) {

            showLoginError( 'Login and password can not be blank' );
            return;
        }

        // console.log( 2, ' login=%s, password = %s', credentials.login, credentials.passw );

        eventer.emit( 'authorize', { login: credentials.login, password: credentials.passw } );

        $scope.loading = true;
        $scope.submitStatus = statuses.loading;
    };


    eventer.on( 'token-ok', function () {

        // console.log( 2, ' token ok ');
        authGranted();
    });


    eventer.on( 'token-fail', function () {

        console.warn( 2, ' token fail ');

        credentials.tokens = null;
        $scope.tokens        = tokens;
        $scope.showLoginForm = false;
        $scope.loading       = false;
        safeApply.run();
    });


    eventer.on( 'tokens-received', function ( tokens ) {
        // console.log( 2, ' Tokens received !!!! ' );
        authGranted( tokens );
    });


    // connect/disconnect events from ws
    eventer.on( 'ws-connected', function () {

        console.log(' +++ ws-connected FIRED');

        $scope.connected = true;
        $scope.submitStatus = statuses.online;
        safeApply.run();

        checkCredentials();

    });


    eventer.on( 'ws-disconnected', function () {
        $scope.connected        = false;
        $scope.showLoginForm    = true;
        $scope.submitStatus     = statuses.offline;
        $rootScope.initializing = false;

        safeApply.run();
    });


    // we should re-send auth request
    eventer.on( 'authToken-expired', function () {

        // console.warn( 2, 'authTokens expired. Trying to request new ones ');

        var creds = localStorage.getItem( 'credentials' );

        try {

            creds = JSON.parse( creds );
            // console.log( 2, 'found creds', creds );

        } catch ( e ) {

            console.warn( 2, ' bad credentials format ');
        }

        if ( creds.login !== 'undefined' && creds.passw !== 'undefined' ) {

            eventer.emit( 'authorize', { login: creds.login, password: creds.passw } );
            $scope.loading = true;
        }
    });

    // wrong login/password
    eventer.on( 'auth-failed', function ( evt ) {

        console.warn( 2, ' Auth failed!!!! ', evt );
        showLoginError( 'Wrong login/password combination. Please try again' );

        $scope.loading = false;
        // tell rootScope that initialization is complete
        $rootScope.initializing = false;
    });


    // logging out
    eventer.on( 'logout', function ( evt ) {
        location.reload(1);
        localStorage.setItem( 'credentials', '' );
    });

}]);