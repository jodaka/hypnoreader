angular.module( 'hr' ).controller( 'ArticleCtrl', [
    '$scope', 'safeApply', '$rootScope', 'eventer',
    function ArticleCtrlFactory( $scope, safeApply, $rootScope, eventer ) {

    $scope.article = {

        visible: false,
        date   : null,
        title  : null,
        author : null,
        text   : null
    };

    $scope.unreadIcon = false;
    $scope.eventer = eventer;

    $scope.originalView = false;

    $scope.DEBUG = false;

    /**
     * [markUnread description]
     * @return {[type]} [description]
     */
    $scope.markUnread = function ( ) {

        if ($scope.DEBUG) console.log( 10, 'marking unread …');
        eventer.emitLocal( 'mark-unread' );
    };

    eventer.on( 'unread-icon', function ( value ) {

        if ($scope.DEBUG) console.log( 10, 'changing unread icon to ', value );

        $scope.unreadIcon = value;
        safeApply.run();
    });


    /**
     * [switchArticle description]
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    var switchArticle = function ( params ) {

        if ( params && params.visible ) {

            if ($scope.DEBUG) console.log( 40, 'switchArticle ', params );
            $scope.article = params;

            $rootScope.articleId = params.id;
            $rootScope.articleOrigin = params.original;

        } else {

            $scope.article.visible = false;
        }
    };


    $scope.applyProxy = function ( ) {
        safeApply.run();
    };

    // when user switches feed we should hide article
    // eventer.on( 'switch-feed', function () {
    //     $scope.article.visible = false;
    // });

    eventer.on( 'switch-article', switchArticle );

    // console.log( 40, 'ArticleCtrl initialized' );


}]);