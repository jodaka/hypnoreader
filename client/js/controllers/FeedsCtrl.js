angular.module( 'hr' ).controller( 'FeedsCtrl', [
    '$scope', 'safeApply', 'dataService', 'eventer', '$rootScope',
    function FeedsCtrlFactory( $scope, safeApply, dataService, eventer, $rootScope ) {

    $scope.feeds       = null;
    $scope.isHidden    = false;

    $scope.hasKbdFocus = true;

    var DEBUG          = false;

    /**
     * Fold/unfold handler
     */
    $scope.foldGroup = function ( group ) {

        group.open = ! group.open;

        // if (DEBUG) console.log( 111, 'folding group ', group );
        safeApply.run();
    };

    /**
     * On pressing SPACE open/close feeds group
     */
    eventer.on( 'toggleFeedGroupOpen', function () {

        var feed = dataService.getFeedOrGroupById( $rootScope.activeFeed );

        if ( typeof feed.feeds !== 'undefined' ) {

            $scope.foldGroup( feed );
        }

    });



    /**
     * Select feed/group
     */
    $scope.selectFeed = function ( feed ) {

        if (DEBUG) console.log( 20, 'selecting feed ', feed, ' activeFeed = ', $rootScope.activeFeed );

        // check if this feed is already selected
        if ( $rootScope.activeFeed === feed.id ) {
            return false;
        }

        var hasKbdFocus = false;

        // deselecting previously active feed
        if ( $rootScope.activeFeed ) {

            var oldSelectedFeed = dataService.getFeedOrGroupById( $rootScope.activeFeed );

            if ( oldSelectedFeed ) {

                hasKbdFocus = oldSelectedFeed.KbdFocus;

                oldSelectedFeed.active = false;
                oldSelectedFeed.KbdFocus = false;
            }

        }

        feed.active = true;
        feed.KbdFocus = hasKbdFocus;

        dataService.setActiveFeed( feed.id );

    };


    // initializing
    // var _init = function () {

    //     console.log( 20, ' initializing ');

    //

    // };

    var drawFeedsList = function () {

        eventer.emitLocal( 'startShortcuts' );

        $scope.feeds = dataService.getUnreadFeeds();

        if (DEBUG) console.log( 'feeds render done ' );

    };


    eventer.on( 'draw-feeds', drawFeedsList );


    /**
     * Marking item as read/unread
     *
     * @param  {[type]} params [description]
     * @return {[type]}        [description]
     */
    var markAs = function ( params ) {

        var tag = params.tag || 'read';
        var inc = ( tag === 'read' ) ? -1 : 1;

        var feed = dataService.getFeedById( params.feedId );

        if ( feed !== null ) {

            feed.unread += inc;

            // decreasing/increasing unread for group
            if ( feed.hasOwnProperty( 'groupID') ) {

                var group = dataService.getGroupById( feed.groupID );
                group.unread += inc;

            }

            $scope.feeds.unreadTotal += inc;

            safeApply.run();
        }
    };

    // increasing/decreasing unread count for feed/group/total
    eventer.on( 'mark-as-read', markAs );
    eventer.on( 'mark-as-unread', markAs );

    eventer.on( 'setKbdFocus', function ( column ) {

        if (DEBUG) console.log( 'activeFeed', $rootScope.activeFeed , ( column === 'feeds' ) );

        var feed = dataService.getFeedOrGroupById( $rootScope.activeFeed );

        feed.KbdFocus = ( column === 'feeds' );

        safeApply.run();

    });

}]);