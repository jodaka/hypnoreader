angular.module( 'hr' ).controller( 'MainCtrl', [
    '$scope', '$rootScope', 'safeApply', 'dataService', 'eventer',
    function MainCtrlFactory( $scope, $rootScope, safeApply, dataService, eventer ) {

    var currentCol = 1;
    var listener   = null;
    var updateTimeout = null;

    var DEBUG = true;

    eventer.on( 'startShortcuts', function () {
        handleShortcuts();
    });

    eventer.on( 'stopShortcuts', function () {

        if ( listener ) {
            document.removeEventListener( listener );
        }

    });


    var handleShortcuts = function () {

        if ( listener !== null ) {
            return;
        }

        listener = document.addEventListener( 'keyup', function ( evt ) {

            if (DEBUG) console.log( 8, ' KEYUP captured', evt.which );

            switch ( evt.which ) {

                // P, K
                case 80:
                case 75:

                    evt.preventDefault();
                    eventer.emitLocal( 'SelectPrevAtom' );
                break;

                // N, J
                case 78:
                case 74:
                    evt.preventDefault();
                    eventer.emitLocal( 'SelectNextAtom' );
                break;

                // left
                case 37:

                    evt.preventDefault();
                    changeColumn( 'left' );

                break;

                // right
                case 39:

                    evt.preventDefault();
                    changeColumn( 'right' );
                break;

                // UP
                case 38:

                    evt.preventDefault();

                    if ( currentCol === 1) {
                        eventer.emitLocal( 'SelectPrevFeed' );
                    }

                    if ( currentCol === 2 ) {
                        eventer.emitLocal( 'SelectPrevAtom' );
                    }

                break;

                // down
                case 40:

                    evt.preventDefault();

                    if ( currentCol === 1) {
                        eventer.emitLocal( 'SelectNextFeed' );
                    }

                    if ( currentCol === 2 ) {
                        eventer.emitLocal( 'SelectNextAtom' );
                    }

                break;

                // SPACE
                case 32:
                    evt.preventDefault();
                    if ( currentCol === 1 ) {
                        eventer.emitLocal( 'toggleFeedGroupOpen' );
                    }
                break;

                case 27:
                    evt.preventDefault();
                    eventer.emitLocal( 'articleCloseOriginal' );
                break;

                // V
                case 86:
                    evt.preventDefault();
                    eventer.emitLocal( 'articleViewOriginal' );
                break;

                // C
                case 67:
                    evt.preventDefault();
                    eventer.emitLocal( 'articleViewOriginalInline' );
                break;

            }

        });
    };

    $scope.updating   = true;
    $scope.share      = {};

    /**
     * Moving keyboard focus from left to right column
     */
    var changeColumn = function ( direction ) {

        if ( currentCol === 2 && direction === 'left' ) {

            if (DEBUG) console.log(' setting KBD focus on FEEDS column');

            // changing selection to FIRST column
            currentCol--;
            eventer.emitLocal( 'setKbdFocus', 'feeds' );

        }

        if ( currentCol === 1 && direction === 'right' ) {

            if (DEBUG) console.log(' setting KBD focus on ATOMS column');

            // changing selection to SECOND column
            currentCol++;
            eventer.emitLocal( 'setKbdFocus', 'atoms' );

        }

    };


    /**
     * Updating feeds
     */
    $scope.updateFeeds = function ( ) {

        if ( $scope.updating ) {
            return;
        }

        $scope.updating = true;
        dataService.updateFeeds();
    };

    /**
     * show/hide feeds panel
     */
    $scope.collapseFeedsPanel = function ( action ) {

        if (DEBUG) console.log( 10, 'collapsing feeds ');

        if (DEBUG) console.log( angular.element( document.getElementById('atoms') ) ) ;
        angular.element( document.getElementById('atoms') ).toggleClass( 'collapsed', action );

        if (DEBUG) console.log( angular.element( document.getElementById('content') ) );
        angular.element( document.getElementById('content') ).toggleClass( 'collapsed', action );

        safeApply.run();
    };

    eventer.on( 'collapse-feeds', $scope.collapseFeedsPanel );

    /**
     * Sending event to remove authorization
     */
    $scope.logout = function () {

        if (DEBUG) console.log(' logging out ');
        eventer.emit( 'logout' );
    };

    /**
     * Timeout for updating data
     */
    var setUpdateTimeout = function () {

        if ( updateTimeout !== null ) {
            clearTimeout( updateTimeout );
        }

        updateTimeout = setTimeout( function triggerUpdate() {

            if (DEBUG) console.log( 10, '---- triggering update ');
            $scope.updateFeeds();

        }, $rootScope.cfg.auto_update_feeds_timeout );
    };

    // done loading
    eventer.on( 'feeds-updated', function updateLoadingStatus() {

        setUpdateTimeout();

        $scope.updating = false;
        safeApply.run();

    });

    // auth stuff
    eventer.on( 'auth-granted', function() {

        if (DEBUG) console.log( 10, ' got auth granted event ');

        $rootScope.authorized = true;
        safeApply.run();

    });

}]);