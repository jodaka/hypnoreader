angular.module( 'hr' ).controller( 'AtomsCtrl', [
    '$scope', 'safeApply', 'eventer', '$rootScope', 'dataService',
    function AtomsCtrlFactory( $scope, safeApply, eventer, $rootScope, dataService ) {

    // $scope.atoms = [];
    $scope.selectedAtom = null;
    $scope.DEBUG = true;

    // var switchFeedParams = null;

    var readTimeout      = null;
    // var updateTimeout    = null;
    var readTimeoutValue = 1 * 500;

    var readItems   = {};
    var unreadItems = {};

    var i = 1;


    /**
     * [selectAtom description]
     * @param  {[type]} atom [description]
     * @return {[type]}      [description]
     */
    $scope.selectAtom = function ( atom ) {

        if ( readTimeout !== null ) {
            clearTimeout ( readTimeout );
            readTimeout = null;
        }

        var hasKbdFocus = false;

        // if we have old active atom
        if ( $scope.selectedAtom ) {

            var oldActiveAtom = dataService.getAtomById( $scope.selectedAtom );

            if ( oldActiveAtom !== null ) {

                hasKbdFocus = oldActiveAtom.KbdFocus;

                oldActiveAtom.active = false;
                oldActiveAtom.KbdFocus = false;
            }
        }

        // console.log( 30, ' ATOM id = ', atom.id, ' selected ');

        // closing original view (if any)
        eventer.emitLocal( 'switch-original', 'close' );

        // changing unread icon
        eventer.emitLocal( 'unread-icon', true );

        $scope.selectedAtom = atom.id;
        atom.active = true;
        atom.KbdFocus = hasKbdFocus;

        eventer.emitLocal( 'switch-article', {

            visible : true,
            date    : atom.ts,
            title   : atom.title,
            author  : atom.author,
            text    : atom.content,
            original: atom.origin

        } );

        // force angular to apply changes
        safeApply.run();

        // return 1;

        // marking atom as read
        readTimeout = setTimeout( function () {

            if ( ! readItems[ atom.id] ) {

                // console.log( 30, ' marking as read atom ', atom.id, atom );
                atom.read            = true;
                readItems[ atom.id ] = true;

                // console.log( 88888, 'atom = ', atom );

                eventer.emit( 'mark-as-read', {
                    feedId: atom.feedId,
                    atomId: atom.id
                });

                eventer.emitLocal( 'unread-icon', false );

                safeApply.run();
            }

            readTimeout          = null;

        }, readTimeoutValue );
    };


    /**
     * [selectAtom description]
     * @param  {[type]} direction [description]
     * @return {[type]}           [description]
     */
    var selectAtomFromKeyboard = function ( direction ) {

        var atom = dataService.getAtomFromSelectedListByDirection( $scope.selectedAtom, direction );


        if ($scope.DEBUG) console.log( 'next-atom ', atom );

        if ( atom !== null ) {
            $scope.selectAtom( atom );
        }

        eventer.emitLocal( 'scroll-atoms-list', direction );
    };

    eventer.on( 'SelectNextAtom', function() {

        // console.warn( 888888888, 'SelectNextAtom fired' );
        selectAtomFromKeyboard('next');

    });
    eventer.on( 'SelectPrevAtom', function() { selectAtomFromKeyboard('prev'); });

    // unread button on the panel was pressed
    eventer.on( 'mark-unread', function (){

        var atom = dataService.getAtomById( $scope.selectedAtom );

        if ( atom ) {

            // var feedId = atom.origin.streamId;
            // console.log( 'UNREAD FOR atom.id ', $scope.selectedAtom, ' feedId=', atom.feedId );

            readItems[ atom.id ] = null;

            eventer.emit( 'mark-as-unread', {
                feedId: atom.feedId,
                atomId: atom.id,
                tag   : 'unread'
            });

            eventer.emitLocal( 'unread-icon', true );
        }

    });

    // renders list of unread items
    var renderUnreadAtomsList = function ( ) {

        $scope.days = dataService.getUnreadAtomsList();
        if ($scope.DEBUG) console.log( ' updated atoms list with new days info ', $scope.days );
        safeApply.run();
    };


    eventer.on( 'unread-atoms-updated', renderUnreadAtomsList );


    // setting/removing Kbd focus
    eventer.on( 'setKbdFocus', function ( column ) {

        if ( $scope.selectedAtom ) {

            console.log(' got active atom ', $scope.selectedAtom, ( column === 'atoms' ) );

            var atom = dataService.getAtomById( $scope.selectedAtom );
            atom.KbdFocus = ( column === 'atoms' );

            safeApply.run();
        }

    });

    // eventer.on( 'items-loaded', updateStoredItems );

    // console.log( 30, 'Atoms initialized ');

}]);