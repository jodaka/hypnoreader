( function () {

    /**
     * Asynchronius script loading
     *
     * @param  {String}   url
     * @param  {Function} [callback]
     * @param  {Number}   [timeout]
     */
    var asyncLoadScript = function ( url, callback, timeout ) {

        var script = document.createElement( 'script' );
        var done = 0;

        // Default error timeout to 10sec
        timeout = ( typeof timeout !== 'undefined' ) ? timeout : 1e4;

        script.src   = url;
        script.async = true;
        script.type  = 'text/javascript';

        if ( typeof callback === 'function' ) {

            script.onreadystatechange = script.onload = function () {

                var rs = script.readyState;

                if ( ! done && (
                                ! rs
                                || rs === 'loaded'
                                || rs === 'complete'
                                || rs === 'uninitialized' )) {

                    // Set done to prevent this function from being called twice.
                    done = 1;
                    callback();

                    // Handle memory leak in IE
                    script.onload = script.onreadystatechange = null;
                }
            };
        }

        // 404 Fallback
        setTimeout( function () {

            if ( ! done ) {

                done = 1;

                // Might as well pass in an error-state if we fire the 404 fallback
                if ( typeof callback === 'function' ) {
                    callback();
                }
            }
        }, timeout );

        // injecting script into document
        var firstScript = document.getElementsByTagName( 'script' )[ 0 ];
        firstScript.parentNode.insertBefore( script, firstScript );

    };

    /**
     * initialization of angular
     */
    var initAngular = function () {

        // console.log(' initialization angular ');
        angular.bootstrap( document.documentElement, [ 'hr' ] );

    };

    /**
     * Displaying error to user
     */
    var fatalError = function ( msg ) {

        // TODO
        // write smth to screen
        // so user could cry
        //
        console.error( msg );
    };


    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

    if ( typeof window.indexedDB === 'undefined' ) {

        // check if there's websql
        if ( typeof window.openDatabase !== 'undefined' ) {

            asyncLoadScript( 'js/IndexedDBShim.min.js', initAngular );

        } else {

            fatalError( 'No indexedDB' );

        }
    }

    if ( 'WebSocket' in window || 'MozWebSocket' in window ) {

        initAngular();
    } else {

        fatalError( 'No WebSockets support' );
    }


}());
