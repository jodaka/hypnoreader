angular.module( 'hr' ).factory( 'dataService', [
    '$rootScope', 'eventer',
    function safeApplyFactory( $rootScope, eventer ) {

    var storage = null;
    var data = {};

    var DEBUG = false;


    var activeFeed = null;

    /**
     * Loading a huge list of all unread items
     */
    var loadUnreadItems = function ( ) {

        // $scope.storage.get( 'unread-atoms', function ( result ) {

        //     // console.log( 30, ' getting items from storage');

        //     // check if items are not outdated
        //     if ( result && result.ts && result.ts + rss.settings.auto_update_feeds_timeout > Date.now() ) {

        //         // console.log( 30, 'WE GOT ITEMS FROM STORAGE. TS= ', result.ts );

        //         // eventer.emitLocal( 'items-loaded', result, 'no-merge' );

        //         updateStoredItems( result );

        //         return;
        //     }

        //     // console.log( 30, 'requesting unread items ' );

        //     // sending command to backend to get items
        //     eventer.emit( 'load-items', '', { xt: 'user/-/state/com.google/read', n: $rootScope.cfg.maxUnreadItemsCount });

        // });
    };


    var handleUnreadAtoms = function ( unreadAtomsData ) {

        // console.log( 30, 'GOT unreadAtomsData TO PARSE', unreadAtomsData, ' timeout ', rss.settings.auto_update_feeds_timeout );

        // var updateAfter = unreadAtomsData.ts + $rootScope.cfg.auto_update_feeds_timeout - Date.now();

        // unreadItems = unreadAtomsData.feeds || [];
        // readItems   = unreadAtomsData.read || [];

        if (DEBUG) console.log( '@@@ ATOMS ', unreadAtomsData );

        data.atoms = {
            ts: unreadAtomsData.ts,
            list: unreadAtomsData.unreadAtoms
        };

        // saving unreadAtomsData
        storage.put({
            id         : 'unread-atoms',
            ts         : unreadAtomsData.ts,
            unreadAtoms: unreadAtomsData.unreadAtoms
        });

        eventer.emit( 'unread-atoms-updated' );
        // if we already have selected feed
        // we would need to update it
        // if ( $rootScope.activeFeed ) {
        //formatSelectedUnreadAtoms();
        // }

        // rss.event.emitLocal( 'unread-updated' );
        // if ( switchFeedParams ) {

            // switchFeed( switchFeedParams );
            // switchFeedParams = null;

        // }

    };

    /**
     * Return atom from current flatSelectedAtomsList
     */
    var getAtomById = function ( id ) {

        if ( typeof id === 'undefined' ) {
            return null;
        }

        for ( var i = 0, iLen = data.flatSelectedAtomsList.length; i < iLen; i++ ) {

            if ( data.flatSelectedAtomsList[ i ].id === id ) {
                return data.flatSelectedAtomsList[ i ];
            }
        }

        return null;
    };

    /**
     * Prepare data structure for AtomsCtrl
     */
    var formatSelectedUnreadAtoms = function() {

        if ( ! $rootScope.activeFeed ) {
            return [];
        }

        var selectedAtoms = [];

        var i, iLen;

        /**
         * Adds feedID and favicon url to each atom
         */
        var augmentAtomsData = function ( list, feedID ) {

            if ( ! list || list.length === 0 ) {
                return [];
            }

            return list.map( function processEachAtom( atom ) {

                atom.feedTitle = data.feedsHash[ feedID ].title;
                atom.favicon   = data.feedsHash[ feedID ].favicon;
                atom.feedId    = feedID;

                return atom;

            });

        };

        // if activeFeed is a group
        if ( data.feedsHash && data.feedsHash[ $rootScope.activeFeed ].group ) {

            var feedsIDsInGroup = data.feedsHash[ $rootScope.activeFeed ].feeds;

            if (DEBUG) console.log( '~~~~~~~==', data.atoms.list );

            for ( i = 0, iLen = feedsIDsInGroup.length; i < iLen; i++ ) {

                if (DEBUG) console.log( '==========', feedsIDsInGroup[ i ], data.atoms.list [ feedsIDsInGroup[ i ] ]);

                // undefined would be when there are no unread atoms for particular feed
                // so we need to skip those
                if ( typeof data.atoms.list !== 'undefined'
                    && typeof data.atoms.list[ feedsIDsInGroup[ i ] ] !== 'undefined' ) {

                    var augmentedList = augmentAtomsData( data.atoms.list[ feedsIDsInGroup[ i ] ], feedsIDsInGroup[ i ] );
                    selectedAtoms = selectedAtoms.concat( augmentedList );
                    if (DEBUG) console.warn( augmentedList );
                }

            }


        } else {
            selectedAtoms = augmentAtomsData( data.atoms.list[ $rootScope.activeFeed ], $rootScope.activeFeed );
        }

        data.flatSelectedAtomsList = selectedAtoms;

        var days      = {};
        var daysKeys  = [];
        var result    = [];

        for ( i = 0, iLen = selectedAtoms.length; i < iLen; i++ ) {

            var _d = new Date( parseInt( selectedAtoms[ i ].ts, 10 ) );

            var _date  = _d.getDate();
            var _month = _d.getMonth();
            var id     = _d.getFullYear() + '-' +
                            ( ( _month < 10 ) ? '0'+_month : _month ) +
                            '-' +
                            ( ( _date < 10 ) ? '0'+_date : _date );


            if ( ! days[id] ) { // faster then using hasOwnProperty

                days[ id ] = {
                    title: parseInt( selectedAtoms[i].ts, 10 ),
                    list: []
                };

                daysKeys.push( id );
            }

            days[ id ].list.push( selectedAtoms[ i ] );

        }

        daysKeys = daysKeys.sort().reverse();

        for ( i = 0, iLen =  daysKeys.length; i < iLen; i++ ) {

            result.push({
                id   : daysKeys[ i ],
                title: days[ daysKeys[ i ] ].title,
                atoms: days[ daysKeys[ i ] ].list
            });
        }

        return result;
        // console.warn( '~~~~~~~~~~~~~~~~~~~~~', result );

        // $scope.days = result;
    };

    eventer.on( 'items-loaded', handleUnreadAtoms );


    // will initialize only after auth is granted
    eventer.on( 'auth-granted', function dataSrvAuthGranted() {

        var _init = function init() {

            storage.get( 'activeFeed', function getStoredActiveFeed( storedAF ) {

                if (DEBUG) console.log( 3, 'got active feed from storage ', storedAF );

                if ( storedAF && typeof storedAF.value !== 'undefined' ) {

                    $rootScope.activeFeed = storedAF.value;
                    //emitActiveFeed();
                }

            });

            // loading feeds cache from storage
            // storage.get( 'feedsList', function processStorageRes( result ) {

            //     console.log( 3, ' got feed list from storage ', result );

            //     if ( result ) {

            //         data.feeds = removeInvisible( result );
            //     }

                // TODO ping feedsCtrl
                // getActiveFeed();
                // safeApply.run();

            // });

            updateData();

        };

        if (DEBUG) console.log( 3, ' got auth granted event ');

        // initializing storage
        storage = new IDBStore({
            onStoreReady: _init,
            storeName   : 'feeds',
            keyPath     : 'id',
            dbVersion   : 2
        });

    });

    /**
     * [selectActiveFeed description]
     * @param  {[type]} af [description]
     * @return {[type]}    [description]
     */
    // var getActiveFeed = function ( ) {

    //     // if we don't have active feed value, lets get it from storage
    //     if ( ! $rootScope.activeFeed ) {

    //         storage.get( 'activeFeed', function ( activeFeed ) {

    //             console.log( 3, 'got active feed from storage ', activeFeed );

    //             if ( activeFeed && typeof activeFeed.value !== 'undefined' ) {

    //                 $rootScope.activeFeed = activeFeed.value;
    //                 emitActiveFeed();
    //             }
    //         });

    //     }
    // };

    /**
     * [emitActiveFeed description]
     * @return {[type]} [description]
     */
    var setActiveFeed = function ( id ) {

        $rootScope.activeFeed = id;

        // saving active feed to storage
        storage.put({
            id   : 'activeFeed',
            value: $rootScope.activeFeed
        });

        // var emitData = {
        //     id: $rootScope.activeFeed
        // };

        // var feed = getFeedOrGroupById( $rootScope.activeFeed );

        // if ( feed.feeds && feed.feeds.length > 0 ) {

        //     var groupFeeds = [];

        //     for ( var i = 0, iLen =  feed.feeds.length; i < iLen; i++ ) {
        //         groupFeeds.push( feed.feeds[i].id );
        //     }

        //     emitData.groupFeeds = groupFeeds;
        // }

        eventer.emit( 'unread-atoms-updated' );

    };



    /**
     * Generating a hash of all feeds for a quick lookups
     * data.feedsHash will have IDs of all feeds
     */
    var makeFeedsHash = function ( ) {

        data.feedsHash = {};

        var processList = function ( list ) {

            for ( var i = 0, iLen = list.length; i < iLen; i++ ) {

                if ( ! data.feedsHash[ list[ i ].id ] ) {

                    data.feedsHash[ list[ i ].id ] = {
                        title  : list[ i ].title,
                        favicon: list[ i ].favicon,
                        unread : list[ i ].unread
                    };
                }

            }

        };

        processList( data.feeds.list );

        var mapIds = function ( list ) {
            return list.map( function ( _feed ) { return _feed.id; } );
        };

        for ( var i = 0, iLen =  data.feeds.groups.length; i < iLen; i++ ) {

            data.feedsHash[ data.feeds.groups[ i ].id ] = {
                title  : data.feeds.groups[ i ].title,
                unread : data.feeds.groups[ i ].unread,
                group  : true,
                feeds  : mapIds( data.feeds.groups[ i ].feeds )
            };

            processList( data.feeds.groups[ i ].feeds );
        }

    };


    var updateData = function () {

        if (DEBUG) console.log( 'SENDING REQUEST TO UPDATE EVERYTHING ');

        // requesting feeds list and unread count
        eventer.emit( 'load-feeds' );

        if (DEBUG) console.log( '@@atoms update');
        // unread items
        eventer.emit( 'load-items', '', { xt: 'user/-/state/com.google/read', n: $rootScope.cfg.maxUnreadItemsCount });
    };

    /**
     * Filter array of data to remove all invisible elements
     * Also apply 'active' flag for currently selected feed ( if any )
     * @param  {Object} data
     * @return {Object}      filtered list with only visible groups/feeds
     */
    var removeInvisible = function ( feedsData ) {

        var unreadCB = function ( value ) {

            // hackery way to mark currently selected active item
            if ( $rootScope.activeFeed && value.id === $rootScope.activeFeed ) {

                value.active = true;
                value.KbdFocus = true;
                console.log( 11111111111111111 )
            }

            return value.unread > 0;
        };

        var result    = {};
        result.list   = feedsData.list.filter( unreadCB );
        result.groups = feedsData.groups.filter( unreadCB );

        // filtering feeds lists in groups
        for ( var i = 0, iLen = result.groups.length; i < iLen; i++ ) {
            result.groups[i].feeds = result.groups[i].feeds.filter( unreadCB );
        }

        result.unreadTotal = feedsData.unreadTotal;

        return result;
    };

    /**
     * Got feeds list from backend
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    var feedsUpdate = function( feedsData ) {

        // console.log( 3, 'got feeds update ');

        feedsData.id      = 'feedsList';
        feedsData.ts      = Date.now();

        // storage.put( feedsData, function () {
        //     console.log( 20, 'feeds saved to storage' );
        // });


        data.feeds = feedsData;
        makeFeedsHash();

        data.unreadFeeds = removeInvisible( feedsData );

        if (DEBUG) console.log( 'DATA', data );

        // tell FeedsCtrl to redraw feedsList
        eventer.emitLocal( 'draw-feeds' );
    };

    // events handling
    eventer.on( 'feeds-updated', feedsUpdate );

    /**
     * [getGroupById description]
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    var getGroupById = function ( id ) {

        for ( var g = 0, gLen = data.feeds.groups.length; g < gLen; g++ ) {

            if ( data.feeds.groups[g].id === id ) {
                return data.feeds.groups[g];
            }
        }
    };

    var getAtomFromSelectedListByDirection = function ( activeAtom, direction ) {


        var getAtomPosition = function ( id ) {

            if ( typeof id === 'undefined' ) {
                return -1;
            }

            for ( var i = 0, iLen = data.flatSelectedAtomsList.length; i < iLen; i++ ) {
                if ( data.flatSelectedAtomsList[ i ].id === id ) {
                    return i;
                }
            }

            return -1;
        };



        if ( ! data.flatSelectedAtomsList || data.flatSelectedAtomsList.length < 1 ) {
            // console.log( 30, ' nowhere to go');
            return;
        }

        var pos = getAtomPosition( activeAtom );

        if ( pos === -1 ) {
            return data.flatSelectedAtomsList[ 0 ];

        }

        // console.log( 30, 'pos = ', pos );

        if ( direction === 'next' ) {

            // NEXT
            if ( pos + 1 < data.flatSelectedAtomsList.length ) {

                return data.flatSelectedAtomsList[ pos + 1 ];

            }

        } else {

            // PREV
            if ( pos - 1 >= 0 ) {

                return data.flatSelectedAtomsList[ pos - 1 ];
            }

        }

        return null;

    };


    /**
     * [getFeed description]
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    var getFeedById = function ( id ) {

        if ( ! id ) {
            return null;
        }

        // looking if feed list
        for ( var i = 0, iLen = data.feeds.list.length; i < iLen; i++ ) {

            if ( data.feeds.list[i].id === id ) {

                return data.feeds.list[i];
            }
        }

        // looking in group
        for ( var g = 0, gLen = data.feeds.groups.length; g < gLen; g++ ) {

            // looking in group members
            for ( i = 0, iLen = data.feeds.groups[g].feeds.length; i < iLen; i++ ) {

                if ( data.feeds.groups[g].feeds[i].id === id ) {

                    data.feeds.groups[g].feeds[i].groupID = data.feeds.groups[g].id;
                    return data.feeds.groups[g].feeds[i];
                }
            }

        }

        return null;
    };

    /**
     * [getFeedOrGroupById description]
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    var getFeedOrGroupById = function ( id ) {

        if ( /^feed/.test( id ) ) {
            return getFeedById( id );
        } else {
            return getGroupById( id );
        }
    };

    return {

        getFeedOrGroupById: getFeedOrGroupById,
        getGroupById      : getGroupById,
        getFeedById       : getFeedById,
        getAtomById       : getAtomById,
        updateFeeds       : updateData,
        getUnreadFeeds    : function() { return data.unreadFeeds; },
        setActiveFeed     : setActiveFeed,
        getUnreadAtomsList: formatSelectedUnreadAtoms,
        getAtomFromSelectedListByDirection: getAtomFromSelectedListByDirection
    };

}]);
