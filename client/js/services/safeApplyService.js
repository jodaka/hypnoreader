angular.module( 'hr' ).factory( 'safeApply', [ '$rootScope', function safeApplyFactory( $rootScope ) {

    return {

        run: function ( fn ) {

            var phase = $rootScope.$$phase;

            if ( phase === '$apply' || phase === '$digest' ) {

                if ( typeof fn  === 'function' ) {
                    fn();
                }

            } else {

                $rootScope.$apply( fn );
            }
        }
    };

}]);
