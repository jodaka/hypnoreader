angular.module( 'hr' ).factory( 'eventer', [
    '$rootScope',
    function eventerFactory( $rootScope ) {

    // basic EventEmitter wrapper to provide websocket events
    var eventer = ( function () {

        var _emitter = new EventEmitter();

        var _emit = function ( args ) {

            var params = Array.prototype.slice.call( arguments, 0 );

            if ( ws ) {
                // sending event over the socket
                ws.send( JSON.stringify( params ) );
            }

            // emitting event localy
            _emitter.emit.apply( this, params );

        };

        return {
            on       : _emitter.on,
            emit     : _emit,
            emitLocal: _emitter.emit
        };

    }());


    // Implement automatic reconnect interface on top of standart
    // websockets API
    var ws                 = null;
    var wsReconnectTimeout = 5 * 1000;
    var wsTimeout          = 2500;
    var wsTimeoutHit       = false;

    var keepAliveInterval  = null;

    var connectWS = function ( isReconnecting ) {

        ws = new WebSocket( $rootScope.cfg.ws_url );

        var localWS = ws;

        var _timeout = setTimeout( function () {

            wsTimeoutHit = true;
            localWS.close();
            wsTimeoutHit = false;

        }, wsTimeout );

        ws.onopen = function ( evt ) {

            clearTimeout( _timeout );
            isReconnecting = false;
            wsOnopen( evt );
        };

        ws.onclose = function ( evt ) {

            clearTimeout( _timeout );
            ws = null;

            if ( ! isReconnecting && ! wsTimeoutHit ) {
                wsOnclose( evt );
            }

            setTimeout( function () {
                connectWS( true );
            }, wsReconnectTimeout);
        };

        ws.onmessage = wsOnmessage;
        ws.onerror   = wsOnerror;
    };

    var wsOnopen = function ( evt ) {
        console.log( 1, ' ws connection established' );

        console.log(' got msg');

        keepAliveInterval = setInterval( function () {

            console.log(' sending ping' );
            ws.send( 'ping' );

        }, 30000 );


        eventer.emit( 'ws-connected' );
    };

    var wsOnclose = function ( evt ) {

        clearTimeout( keepAliveInterval );

        console.log( 1, ' ws connection closed ');
        eventer.emit( 'ws-disconnected' );
    };

    var wsOnmessage = function ( evt, flags ) {

        var data = [];
        try {
            data = JSON.parse( evt.data );
        } catch(e) {
            console.log( evt );
            console.warn( 1, 'unable to parse ws msg', evt.data, evt );
        }

        if ( data.eventer ) {
            console.log( 1, 'ws eventer msg:', data.eventer[0], data );
            eventer.emit.apply( eventer, data.eventer );
        } else {
            console.warn( 1, 'unknown msg ', data, evt.data, evt );
        }

    };

    var wsOnerror = function ( evt ) {
        console.error( evt );
    };

    // let's check if we are in node-webkit env
    if ( typeof process !== 'undefined' && process.versions && process.versions.node ) {

        console.log( '(((( node-webkit ENV detected ))))')
        eventer = rss.eventer;
        eventer.resume();

        setTimeout( function() {
            eventer.emitLocal( 'ws-connected' );
        }, 100);

    } else {

        // or in pure client server
        // connecting
        connectWS();
    }



    return eventer;

}]);