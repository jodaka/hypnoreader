
( function() {

    'use strict';

    var iconv = require('iconv');

    // var deferred = require('deferred');
    var request  = require('request');

    // var settings = require('config').settings;
    var log      = require('logule').init( module, 'proxy' );

    var eventer        = null;


    var init = function ( params ) {

        eventer = params.events;
        // auth    = params.auth;

        eventer.on( 'proxy-page', proxyPage );

    };


    /**
     * [proxyPage description]
     * @param  {[type]} url [description]
     * @return {[type]}     [description]
     */
    var proxyPage = function ( url ) {

        log.debug( ' --> requested ' + url );

        if ( ! url ) {

            log.error(' proxyPage called with empty url ', url );
            return false;
        }

        request.get({
            url     : url,
            encoding: 'binary'
        }, function parseProxyResponse( err, req, resp ) {

            log.debug( ' <-- requested ' + url + ' loaded ');

            if ( req.statusCode === 200 ) {

                // we can't just use url from original request
                // because there could be redirects (often happens with feedburner urls)
                var hostname = req.request.uri.protocol + '//' + req.request.uri.hostname + ':' + req.request.uri.port + '/';

                var charset = 'utf-8';

                if ( req.headers['content-type'] && /harset/.test( req.headers['content-type'] ) ) {
                    charset = req.headers['content-type'].replace( /.*charset=(.*?)$/, "$1" );
                }

                var responseBody = '';
                var responseBuffer = new Buffer( resp, 'binary' );

                // if we don't have UTF-8, then we'll need to recode
                if ( charset !== 'utf-8' ) {

                    log.trace(' GOT ENCODING ', charset );

                    var conv = new iconv.Iconv( charset, 'utf8');

                    responseBody = conv.convert( responseBuffer ).toString();

                } else {

                    log.trace(' NOT CHANGING ENCODING ', charset );

                    responseBody = responseBuffer.toString();

                }

                // fixing all relative links
                responseBody = responseBody.replace(  /<head(?:\s.*?)?>/i, '<head><base href="' + hostname + '/">' );

                eventer.emit( 'proxy-page-loaded', { content: responseBody, charset: charset } );

            } else {

                log.error( ' <-- requested ' + url + ' loaded ' );
                log.error( req );
                log.error( resp );
                eventer.emit( 'proxy-page-loaded', " Can't load " + url );

            }

        });

    };

    module.exports = {
        init     : init
    };

}());