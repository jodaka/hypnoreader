
( function() {

    "use strict";

    var deferred = require('deferred');
    var request  = require('request');
    var urls     = require('config').readerAPI;
    var settings = require('config').settings;

    var log       = require('logule').init( module, 'feeds' );

    var eventer        = null;
    var auth           = null;
    var cfg            = null;

    var userPrefs      = null;
    var userInfo       = null;
    var feeds          = null;

    var updateInterval = null;

    /**
     * Unerscore's extend
     */
    var _extend = function(target) {
            var i = 1;
            var length = arguments.length;
            var source;
            var undef;

            for ( ; i < length; i++ ) {
                // Only deal with defined values
                if ( (source = arguments[i]) !== undef ) {

                    Object.getOwnPropertyNames(source).forEach(
                        function(k){
                           var d = Object.getOwnPropertyDescriptor(source, k) || {value:source[k]};
                           if (d.get) {
                               target.__defineGetter__(k, d.get);
                               if (d.set) target.__defineSetter__(k, d.set);
                           }
                           else if (target !== d.value) {
                               target[k] = d.value;
                           }
                       });
                }
            }
            return target;
        };

    /**
     * TODO
     * @return {[type]} [description]
     */
    var getRequestParams = function () {

        return {
            ck           : Date.now(),
            client       : settings.client_name,
            output       : 'json',
            service      : 'reader',
            accountType  : 'GOOGLE'
        };
    };

    /**
     * Storing references for auth and storage
     *
     * @return {[type]}         [description]
     */
    var init = function ( params ) {

        eventer = params.events;
        auth    = params.auth;

        eventer.on( 'load-items', loadItems );
        eventer.on( 'load-feeds', loadFeeds );
        eventer.on( 'mark-as-read', markAs );
        eventer.on( 'mark-as-unread', markAs );

        log.info( ' --- feeds initialized ');
        // eventer.on( 'ws-disconnect', function () {
        //     clearInterval( updateInterval );
        // });


        // when authorization received we should start updating our feeds
        // eventer.on( 'auth-granted', function () {

        //     log.info(' subscribing to update loadFeeds each %s seconds ', Math.round( settings.auto_update_feeds_timeout/ 1000 ) );

        //     updateInterval = setInterval( function (){

        //         log.info(' triggering loadFeeds on interval ');
        //         loadFeeds();

        //     }, settings.auto_update_feeds_timeout );
        // });

        // eventer.on( 'auth-expired', function () {
        //     clearInterval( updateInterval );
        // });

    };

    /**
     * [markAsRead description]
     * @param  {[type]} params [description]
     * @return {[type]}        [description]
     */
    var markAs = function ( params ) {

        var tag = params.tag || 'read';
        log.info(' req URL ', urls.base + urls.suffixes.edit_tag );

        var postData = {
            i: params.atomId,
            a: urls.tags[tag],
            s: params.feedId,
            T: auth.accessToken()
        };

        // we need to remove 'read' tag
        if ( tag === 'unread' ) {
            postData.r = urls.tags.read;
        }

        log.info( 'post data', postData );

        request.post({
            headers: authTokens(),
            url    : urls.base + urls.suffixes.edit_tag,
            form   : postData
        }, function ( err, req, resp ) {

            if ( req && req.statusCode === 200 ) {

                log.info( 'Mark-as-' + tag + ' succeed for atom ', params.atomId );
                return;
            }

            log.error( ' ***** got ERROR ' + err );
            log.warn( "status =" + req.statusCode );
            log.warn( req.headers );

        });

    };

    /**
     * Requesting user preferences
     * @return {[type]} [description]
     */
    var getUserInfo = function ( tokens ) {

        var def = deferred();

        log.warn( ' >> requesting user info', tokens );

        request( {
            headers: authTokens(),
            method : 'GET',
            url    : urls.base + urls.suffixes.user_info,
            qs     : getRequestParams()
        }, function ( err, req, resp ) {

            if ( req && req.statusCode === 200 ) {

                userInfo = JSON.parse( resp );
                def.resolve( userInfo );
                return;
            }

            log.warn( ' ***** got ERROR ', err );
            def.resolve( new Error( err ) );
        });

        return def.promise;
    };

    /**
     * Requesting user preferences
     * @return {[type]} [description]
     */
    var getUserPreferences = function ( ) {

        var def = deferred();

        if ( userPrefs !== null ) {

            def.resolve( userPrefs );

        } else {

            log.warn( ' >> requesting user prefs' );

            request( {

                headers: authTokens(),
                method : 'GET',
                url    : urls.base + urls.paths.preferences,
                qs     : getRequestParams()

            }, function ( err, req, resp ) {

                if ( req && req.statusCode === 200 ) {

                    userPrefs = JSON.parse( resp ).streamprefs;
                    def.resolve( userPrefs );
                    return;

                }

                def.resolve( new Error( resp ) );
            });

        }

    };

    /**
     * Loading of feeds
     * @return {promise}
     */
    var updateFeeds = function () {

        var def = deferred();

        log.info(' >> loading feeds ');

        request( {

            headers: authTokens(),
            method : 'GET',
            url    : urls.base + urls.paths.subscription + urls.suffixes.list,
            qs     : getRequestParams()

        }, function ( err, req, resp ) {

            if ( req && req.statusCode === 200 ) {

                log.info( " << got feeds. Status code " + req.statusCode + ". Parsing ");
                log.trace( resp );

                var rawList = JSON.parse( resp ).subscriptions;
                def.resolve( rawList );
                return;

            }

            log.error( ' ***** got ERROR ',  resp );
            def.resolve( new Error( resp ) );
        });

        return def.promise;
    };

    /**
     * TODO
     * @return {promise}
     */
    var updateTags = function () {

        var def = deferred();

        log.info(' >> loading tags ');

        request( {
            headers: authTokens(),
            method : 'GET',
            url    : urls.base + urls.paths.tags + urls.suffixes.list,
            qs     : getRequestParams()
        }, function ( err, req, resp ) {

            if ( req && req.statusCode === 200 ) {

                log.info( " << got tags ");
                def.resolve( resp );
                return;
            }


            log.error( ' ***** got ERROR ',  resp );
            def.resolve( new Error( resp ) );

        });

        return def.promise;
    };

    /**
     * TODO
     * @return {promise}
     */
    var updateUnreadCount = function () {

        var def = deferred();

        log.info(' >> loading unread count ');

        request( {
            headers: authTokens(),
            method : 'GET',
            url    : urls.base + urls.suffixes.unread_count,
            qs     : getRequestParams()
        }, function ( err, req, resp ) {

            if ( req && req.statusCode === 200 ) {

                var unreadData = JSON.parse( resp ).unreadcounts;
                log.info( " << got unread ");

                def.resolve( unreadData );
                return;

            }

            log.warn( ' ***** got ERROR ',  resp );
            def.resolve( new Error( resp ) );
        });

        return def.promise;
    };


    /**
     * just a shortcut
     * @return {Object} auth tokens
     */
    var authTokens = function () {
        return {
            Authorization : "GoogleLogin auth=" + auth.authToken(),
            T             : auth.accessToken()
        };
    };

    /**
     * Loading atoms for a particular feed
     * @param  {String} url    feed id
     * @param  {Object} params [description]
     * @return {Object}
     */
    var loadItems = function ( url, params ) {

        log.info(' >> loading items for URL ' + url );

        // ot=[unix timestamp] : The time from which you want to retrieve items. Only items that
        // have been crawled by Google Reader after this time will be returned.
        //
        // r=[d|n|o] : Sort order of item results. d or n gives items in descending date order,
        // o in ascending order.
        //
        // xt=[exclude target] : Used to exclude certain items from the feed. For example,
        // using xt=user/-/state/com.google/read will exclude items that the current user
        // has marked as read, or xt=feed/[feedurl] will exclude items from a particular
        // feed (obviously not useful in this request, but xt appears in other listing requests).
        //
        // ot=[unix timestamp] : The time from which you want to retrieve items. Only items that
        // have been crawled by Google Reader after this time will be returned.
        //
        // n=[integer] : The maximum number of results to return. 20 by default

        params =  _extend( getRequestParams(), params );

        request.get({
            headers: authTokens(),
            url    : urls.base + urls.paths.stream + encodeURIComponent( url ),
            qs     : params
        }, function ( err, req, resp ) {

            if ( err ) {

                log.warn( ' ***** got ERROR ',  resp );
                return false;
            }

            // auth fail
            if ( req.statusCode === 400 ) {

                eventer.emit( 'authToken-expired' );
                // log.info( " ... planning to rerun load-items request form " + url );
                // eventer.emit( 'load-items', url, params );
            }

            if ( req.statusCode === 200 ) {

                try {
                    resp = JSON.parse( resp );
                } catch ( e ) {

                    log.error( 'Unable to parse atoms response ', e );
                    log.error( resp );
                    eventer.emit( 'items-loaded', { id: url, data: { id: url, items: [] } } );
                    return false;
                }

                // strip unnecessary data
                var result = {
                    continuation: resp.continuation || '',
                    ts          : Date.now(),
                    url         : url
                };

                var feeds = {};

                log.info( 'got a total of '+resp.items.length+' items ');

                for ( var i = 0, iLen =  resp.items.length; i < iLen; i++ ) {

                    var feedID = resp.items[i].origin.streamId;

                    if ( ! feeds[ feedID ] ) {
                        feeds[ feedID ] = [];
                    }

                    // log.info( 'streamID =', feedID );

                    var content = ( resp.items[i].summary )
                        ? resp.items[i].summary.content
                        : ( resp.items[i].content )
                            ? resp.items[i].content.content
                            : '';

                    feeds[ feedID ].push({
                        id       : resp.items[i].id,
                        ts       : resp.items[i].crawlTimeMsec,
                        origin   : resp.items[i].alternate && resp.items[i].alternate[0].href || '',
                        // feedTitle: resp.items[i].origin.title,
                        // feedId   : feedID,
                        title    : resp.items[i].title,
                        author   : resp.items[i].author || '',
                        content  : content
                    });
                }

                result.unreadAtoms = feeds;

                eventer.emit( 'items-loaded', result );
            }

        });

    };

    /**
     * TODO
     * @return {promise}
     */
    var loadFeeds = function () {

        var def = deferred();

        deferred(

            updateFeeds(),
            updateUnreadCount()
            // getUserPreferences(),
            // updateTags(),

        ).end( function ( result ) {


            // result[0] is Feeds
            // result[1] is UnreadCount

            feeds = {
                list       : [],
                groups     : [],
                unreadTotal: 0
            };

            var rawList        = result[0];
            var unreadData     = result[1];

            var rawLookup      = {};
            var groupsLookup   = {};
            var unreadNotFeeds = [];

            //var domainRE = /.*?\/(http[s]?:\/\/\S*?)\/.*/;

            // creating RAW lookup
            for ( var i = 0, iLen = rawList.length; i < iLen; i++ ) {
                rawLookup[ rawList[i].id ] = i;
                //rawList[i].domain = rawList[i].id.replace( domainRE, "$1" );
            }

            // merging unread items for feeds
            for ( i = 0, iLen = unreadData.length; i < iLen; i++ ) {

                if ( /^feed/.test( unreadData[i].id ) ) {

                    var feedPosition = rawLookup[ unreadData[i].id ];

                    rawList[ feedPosition ].unread = unreadData[i].count;
                    rawList[ feedPosition ].ts     = unreadData[i].newestItemTimestampUsec;

                    feeds.unreadTotal += unreadData[i].count;

                } else {
                    unreadNotFeeds.push( unreadData[i] );
                }
            }

            // orginizing feeds and groups
            for ( i = 0, iLen = rawList.length; i < iLen; i++ ) {

                // feeds.lookup[ rawList[i].id ] = [ 'list', i ];
                if ( rawList[i].categories.length > 0 ) {

                    for ( var c = 0, clen = rawList[i].categories.length; c < clen; c++ ) {

                        if ( ! groupsLookup.hasOwnProperty( rawList[i].categories[c].id ) ) {

                            feeds.groups.push({
                                id    : rawList[i].categories[c].id,
                                title : rawList[i].categories[c].label,
                                feeds : [],
                                unread: 0
                            });

                            groupsLookup[ rawList[i].categories[c].id ] = feeds.groups.length - 1;
                        }

                        feeds.groups[ groupsLookup[ rawList[i].categories[c].id ] ].feeds.push({
                            id    : rawList[i].id,
                            title : rawList[i].title,
                            ts    : rawList[i].ts || rawList[i].firstitemmsec,
                            unread: rawList[i].unread || 0,
                            domain: rawList[i].domain
                        });

                    }

                } else {

                    feeds.list.push({
                        id    : rawList[i].id,
                        title : rawList[i].title,
                        ts    : rawList[i].ts || rawList[i].firstitemmsec,
                        unread: rawList[i].unread || 0,
                        domain: rawList[i].domain
                    });
                }

            }

            // merging group unreads
            for ( i = 0, iLen = feeds.groups.length; i < iLen; i++ ) {

                var groupUnread = 0;

                for ( var f = 0, fLen = feeds.groups[i].feeds.length; f < fLen; f++ ) {
                    groupUnread += feeds.groups[i].feeds[f].unread;
                }

                feeds.groups[i].unread = groupUnread;
            }

            eventer.emit( 'feeds-updated', feeds );
            def.resolve( 'ok' );

        }, function ( err ) {

            log.error( err );
            def.resolve( err );
        } );

        return def.promise;
    };

    module.exports = {
        loadFeeds: loadFeeds,
        loadItems: loadItems,
        init     : init
    };

}());