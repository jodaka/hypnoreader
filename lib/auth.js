(function (){

    'use strict';

    var request  = require('request');
    var urls     = require('config').readerAPI;
    var settings = require('config').settings;

    var log       = require('logule').init( module, 'auth');
    var eventer   = null;

    // local vars
    var auth      = {};
    var tokensUpdateInterval = null;

    /**
     * [init description]
     * @param  {[type]} params [description]
     * @return {[type]}        [description]
     */
    var init = function ( params ) {

        eventer = params.events;

        log.info( ' -- Auth initialized' );

        eventer.on( 'authorize', authorize );

        eventer.on( 'logout', logout );

        eventer.on( 'token-ok', scheduleTokenUpdate );

        /**
         * Stop periodical tokens updates when user disconnects
         */
        eventer.on( 'ws-disconnect', function () {

            log.info( ' == clearing update interval ' );
            clearInterval( tokensUpdateInterval );
        });

        /**
         * Tokens checking is performed by requesting user info
         */
        eventer.on( 'check-token', checkTokens );

    };

    var logError = function ( err, req, resp, msg, emit ) {

        log.error( ' !!! ERROR ', msg );
        log.error( 'pesponse code ', req && req.statusCode );

        log.error( err );
        log.error( resp );

        if ( emit ) {
            eventer.emit( emit );
        }
    };

    /**
     * Performing tokens check by requesting user info and checking for userId existance
     * @param  {String} tokens.authToken
     * @param  {String} tokens.accessToken
     * @return {Event}        emits either 'token-ok' or 'token-fail'
     */
    var checkTokens = function ( tokens ) {

        log.info( ' checking tokens … ');

        request({
            method : 'GET',
            url    : urls.base + urls.suffixes.user_info,
            headers: {
                Authorization : "GoogleLogin auth=" + tokens.authToken,
                T             : tokens.accessToken
            },
            qs: {
                ck           : Date.now(),
                client       : settings.client_name,
                output       : 'json',
                service      : 'reader',
                accountType  : 'GOOGLE'
            }
        },
        function ( err, req, resp ) {

            if ( err || req.statusCode !== 200 ) {

                logError( err, req, resp, 'Tokens check failed', 'token-fail' );
                return;
            }

            var userInfo = JSON.parse( resp );

            // check that we actually has some meaningfull data
            // there should be userId
            if ( userInfo.userId ) {

                log.info( ' tokens OK ');

                // saving tokens locally
                auth.authToken   = tokens.authToken;
                auth.accessToken = tokens.accessToken;

                eventer.emit( 'token-ok', tokens );
                return;
            }

            logError( err, req, resp, 'token check failed. Parsed server response don\'t have userId in the response', 'token-fail' );


        });

    };

    /**
     * Requesting Auth token from google using deprecated (!) clientLogin :)
     *
     * @param  {String} login
     * @param  {String} password
     * @return {String}          auth string
     */
    var getAuthToken = function ( login, password, cb ) {

        log.info( ' )) requesting new auth. login=' + login + ' password =' + password );

        // Sending request for Auth token
        request({
            method: 'GET',
            url   : urls.login,
            qs: {
                Email      : login,
                Passwd     : password,
                accountType: 'GOOGLE',
                service    : 'reader',
                output     : 'json',
                client     : 'yareader',
                ck         : Date.now()
            }
        },
        function ( err, req, resp ) {

            if ( err ) {
                log.error( err );
                return;
            }

            log.warn( 'getAuthToken request status = ', req.statusCode );

            // checking for wrong password
            if ( req.statusCode === 403 ) {

                // report to client that we have error
                eventer.emit( 'auth-failed', 'bad-login-or-password' );
                // def.resolve( new Error(' ***** BAD AUTH login=' + login +' password =' + password ) );
                log.error( ' ***** BAD AUTH login=' + login +' password =' + password );

                return;
            }

            // parsing response.
            // it consists of 3 lines:
            // SID=...
            // LSID=...
            // Auth=...
            var token = resp.split( "\n" )[2].replace( "Auth=", '' );

            if ( typeof cb === 'function' ) {
                cb( token );
            }

        });

    };

    /**
     * [getAccessToken description]
     * @return {[type]} [description]
     */
    var getAccessToken = function ( cb ) {

        log.info( ' >> making access token request with token ', auth.authToken );

        request({
            method: 'GET',
            url   : urls.base + urls.suffixes.token,
            headers: {
                "Authorization": "GoogleLogin auth=" + auth.authToken
            }
        },
        function ( err, req, resp ) {

            log.info( ' << got access token response  resp==', resp );

            if ( err || req.statusCode !== 200 ) {

                logError( err, req, resp, 'Tokens check failed', 'token-fail' );
                return;
            }

            if ( typeof cb === 'function' ) {
                cb( resp );
            }

        });
    };


    /**
     * [logout description]
     * @return {[type]} [description]
     */
    var logout = function () {

        auth = {};
    };


    /**
     * Emitting locally saved tokens to all listeners of 'tokens-received' event
     */
    var announceTokens = function () {

        // propagating Auth ready event
        eventer.emit( 'tokens-received', {
            authToken  : auth.authToken,
            accessToken: auth.accessToken
        });

    };


    /**
     * Clearing update interval
     */
    var cancelTokensUpdate = function () {

        // resetting updates
        if ( tokensUpdateInterval !== null ) {

            log.info( '======= clearing update interval ');
            clearInterval( tokensUpdateInterval );
            tokensUpdateInterval = null;
        }
    };


    /**
     * updating accessToken by timeout
     * every 25 minutes ( they would expire in 30 mins )
     */
    var scheduleTokenUpdate = function () {

        cancelTokensUpdate();

        tokensUpdateInterval = setInterval( function () {

            log.info( 'timeout fired');

            getAccessToken( function ( accessToken ) {

                log.info( ' (( accessToken updated', accessToken );
                auth.accessToken = accessToken;
                announceTokens();
            });

        }, 25 * 60 * 1000 );
    };

    /**
     * [authorize description]
     * @param  {[type]} login    [description]
     * @param  {[type]} password [description]
     * @return {[type]}          [description]
     */
    var authorize = function ( params ) {

        log.info(' -- starting authorize ');

        cancelTokensUpdate();

        // getting Auth Token
        getAuthToken( params.login, params.password, function ( authToken ) {

            log.info( ' (( authToken received', authToken );
            auth.authToken = authToken;

            getAccessToken( function ( accessToken ) {

                log.info( ' (( accessToken received', accessToken );
                auth.accessToken = accessToken;

                announceTokens();
                scheduleTokenUpdate();

            });

        });

    };

    module.exports = {
        init       : init,
        authToken  : function(){ return auth.authToken;   },
        accessToken: function(){ return auth.accessToken; }
    };

}());