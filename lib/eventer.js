/**
 * Simple event emitter wrapper with queue and websockets support
 */
(function () {

    'use strict';

    var log       = require('logule').init( module, 'eventer' );
    log.info(' -- CREATING NEW EVENTER');

    var e = {
        emitter: ( new ( require( 'events' ).EventEmitter )() ),
        queue  : [],
        state  : 'paused',
        ws     : null
    };

    /**
     * Switch eventer state between paused/resume state.
     * If changed to resumed state, will process queued events;
     *
     * @param  {String} state paused/resumed
     */
    e.changeState = function ( state ) {

        if ( state === 'resumed' && e.queue.length > 0 ) {

            while ( e.queue.length ) {

                e.emitter.emit.apply( this, e.queue.splice( 0, 1 ) );
            }

        }

        e.state = state;

    };

    /**
     * Saves a reference to websocket instance
     * @param  {[type]} ws [description]
     */
    e.websocket = function ( ws ) {

        if ( typeof ws !== "undefined" ) {
            e.ws = ws;
            e.changeState('resumed');
        }

    };

    /**
     * Simple emit wrapper that either emit events or put them into
     * queue if eventer is in paused state
     * @param  {String} event
     * @param  {Object} data
     */
    e.emitLocal = function () {


        var params = Array.prototype.slice.call( arguments, 0 );
        var evt = params.slice( 0, 1 )[0];

        if ( evt === 'newListener' ) {
            return;
        }

        if ( e.state === 'paused' ) {

            e.queue.push( params );

        } else {
            log.debug(' ~~~ emiting ', params );
            e.emitter.emit.apply( this, params );
        }

    };

    /**
     * Wrapper that will emit event and send a copy over websocket if later is available
     */
    e.emitToWebsocket = function() {

        e.emitLocal.apply( this, arguments );

        if ( e.ws ) {

            var params = Array.prototype.slice.call( arguments, 0 );

            log.debug(' sending data to socket ');

            e.ws.send( JSON.stringify({ eventer: params }), function ( err ) {
                // probably client has already disconnected, so it's not a
                // real error in most cases
                log.warn( e );
            });
        }
    };

    // pause/resume evented interface
    e.emitter.on( 'eventer-pause', function () { e.changeState('paused'); });
    e.emitter.on( 'eventer-resume', function () { e.changeState('resumed'); });

    module.exports = {
        on       : e.emitter.on,
        emitLocal: e.emitLocal,
        emit     : e.emitToWebsocket,
        state    : function () { return e.state; },
        pause    : function () { e.changeState('paused'); },
        resume   : function () { e.changeState('resumed'); },
        ws       : e.websocket
    };

}());

